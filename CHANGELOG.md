# OMC3-kmod Changelog


## 21-02-2023 V0.2.6
- fix issue with parsing nan and array in ini files

## 07-02-2023 V0.2.5
- reintroduce phaseweight option and tests

## 27-04-2022 V0.2.4
- drop nan in input after saving inputs to config file, add IP1 and IP5 measurements and tests

## 25-04-2022 V0.2.2
- temporary fix sign of Beam2 K

## 19-04-2022 V0.2.0
- move workflow to acc-py for release

## 14-10-2021 V0.1
- adapt script for new format of data provided by GUI


## 31-08-2021 
- extract K-Mod scripts from omc3 and move to separate repository
