import tfs

from pathlib import Path
from generic_parser import EntryPointParameters, entrypoint
from generic_parser.entry_datatypes import DictAsString
from omc3_kmod.run_kmod import analyse_kmod
from omc3_kmod.kmod.constants import (INSTRUMENT_NAMES, INSTRUMENT_LOCATIONS)
from omc3_kmod.utils import iotools, logging_tools

LOG = logging_tools.get_logger(__name__)

def kmod_params():
    parser = EntryPointParameters()
    parser.add_parameter(name='files',
                         type=str,
                         nargs='+',
                         required=True,
                         help='String with Paths to the outputfiles of K-Modulation')
    parser.add_parameter(name='beam',
                         type=int,
                         choices=[1, 2], required=True,
                         help='define beam used: 1 or 2',)
    parser.add_parameter(name='betastar',
                         type=float,
                         nargs='+',
                         help='Estimated beta star of measurements',)
    parser.add_parameter(name='waist',
                         type=float,
                         nargs='+',
                         help='Estimated waist shift',)
    parser.add_parameter(name='cminus', 
                         type=float,                         
                         help='C Minus',)
    parser.add_parameter(name='misalignment',
                         type=float,
                         help='misalignment of the modulated quadrupoles in m',)
    parser.add_parameter(name='errorK',
                         type=float,
                         help='error in K of the modulated quadrupoles, relative to gradient',)
    parser.add_parameter(name='errorL',
                         type=float,                         
                         help='error in length of the modulated quadrupoles, unit m',)
    parser.add_parameter(name='tune_uncertainty',
                         type=float,
                         default=2.5e-5,
                         help='tune measurement uncertainty')
    parser.add_parameter(name='instruments',
                         type=DictAsString,
                         help='Dict containing name and s-location of instruments at which optics should be evaluated.'\
                              f'Example: {{"{INSTRUMENT_NAMES}":[BGI, BWS, IP], "{INSTRUMENT_LOCATIONS}":[1, 2, 3]}}'\
                              'Caveat: the user is responsible to check that the locations lie inbetween the two quadrupoles.',)
    parser.add_parameter(name='no_autoclean',
                         action='store_true',
                         help='flag for manually cleaning data')
    parser.add_parameter(name='no_sig_digits',
                         action='store_true',                         
                         help='flag to not use significant digits')
    parser.add_parameter(name='plots',
                         action='store_true',                         
                         help='flag to create any plots')
    parser.add_parameter(name='phase_files',
                         type=Path,
                         nargs=2,
                         help='Paths to the phase twiss files, either from model or measurement. One per plane, first file for X, second for Y.')
    parser.add_parameter(name='bpms',
                         type=str,
                         nargs=2,
                         help='BPMs to use for phase weight')
    parser.add_parameter(name='phase_weight',
                         type=float,
                         default=0.0,
                         help='weight in penalty function between phase and beta.'
                              'If weight=0 phase is not used as a constraint.')
    parser.add_parameter(name="outputdir",
                         type=Path,
                         help="Path where outputfiles will be stored, defaults "
                                                "to the given working_directory")

    return parser

@entrypoint(kmod_params(), strict=True)
def analyse_kmod_files(opt):
    LOG.info('Getting input parameter')
    iotools.save_config(opt.outputdir, opt, "omc3-kmod-load-files")

    paths = opt.pop('files')

    circuits={}
    for path in paths:
        magnet_df = tfs.read(path)

        circuits[eval(magnet_df.headers['MAGNETNAMES'])[0]]={
                'MAGNETNAMES': eval(magnet_df.headers['MAGNETNAMES']),
                'MAGNETLENGTHS':eval(magnet_df.headers['MAGNETLENGTHS']),
                'MAGNETLOCATIONS':eval(magnet_df.headers['MAGNETLOCATIONS']),
                'circuitsK':magnet_df['K'],
                'qh1':magnet_df['TUNEX'],
                'err_qh1':magnet_df['ERRTUNEX'],
                'qv1':magnet_df['TUNEY'],
                'err_qv1':magnet_df['ERRTUNEY'],
                'qh2':magnet_df['TUNEX'],
                'err_qh2':magnet_df['ERRTUNEX'],
                'qv2':magnet_df['TUNEY'],
                'err_qv2':magnet_df['ERRTUNEY'],
        }
    opt['circuits']=circuits
    return analyse_kmod(opt)

if __name__ == "__main__":
    analyse_kmod_files()