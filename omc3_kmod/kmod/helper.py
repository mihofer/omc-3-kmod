"""
Helper
------

This module contains helper functionality for ``kmod``.
It provides functions to perform data cleaning, IO loading and plotting.
"""

import numpy as np
from matplotlib import pyplot as plt

from omc3_kmod.definitions.constants import PLANES
from omc3_kmod.kmod import analysis
from omc3_kmod.kmod.constants import (ERR, TUNE, CLEANED, K, AVERAGE, BETA)
from omc3_kmod.utils import logging_tools, outliers

LOG = logging_tools.get_logger(__name__)


def clean_data(magnet_df, no_autoclean):
    if no_autoclean:
        LOG.info('Manual cleaning is not yet implemented, no cleaning was performed')
        for plane in PLANES:
            magnet_df[f"{CLEANED}{plane}"] = True
    else:
        LOG.debug('Automatic Tune cleaning')
        for plane in PLANES:
            magnet_df[f"{CLEANED}{plane}"] = outliers.get_filter_mask(
                magnet_df[f"{TUNE}{plane}"].values, x_data=magnet_df[K].values, limit=1e-5)
    return magnet_df


def add_tune_uncertainty(magnet_df, tune_uncertainty):
    LOG.debug(f'Adding {tune_uncertainty} units tune measurement uncertainty')
    for plane in PLANES:
        magnet_df[f"{ERR}{TUNE}{plane}"] = np.sqrt(magnet_df[f"{ERR}{TUNE}{plane}"]**2 + tune_uncertainty**2)
    return magnet_df


# ##############################    PLOTTING    #############################################


def plot_cleaned_data(magnet_dfs, plot_name, interactive_plot=False):
    fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(10, 10))
    for i, plane in enumerate(PLANES):
        for j in range(2):
            ax_plot(ax[i, j], magnet_dfs[j], plane)
    ax[1, 1].legend()
    plt.tight_layout()
    plt.savefig(plot_name)
    if interactive_plot:
        plt.show()
    return fig


def ax_plot(ax, magnet_df, plane):
    ax.set_title(magnet_df.headers['MAGNETNAMES'], fontsize=15)

    ax_errorbar_plot(ax=ax, magnet_df=magnet_df, plane=plane, clean=True,
                     plot_settings={"color": "blue", "marker": "o", "label": "Data", "zorder": 1})
    ax_errorbar_plot(ax=ax, magnet_df=magnet_df, plane=plane, clean=False,
                     plot_settings={"color": "orange", "marker": "o", "label": "Cleaned", "zorder": 2})
    ax.plot(return_delta_k(magnet_df.loc[magnet_df.loc[:, f"{CLEANED}{plane}"].values == True, :]),
            return_fit_data(magnet_df.loc[magnet_df.loc[:, f"{CLEANED}{plane}"].values == True, :], plane),
            color='red', label='Fit', zorder=3)
    ax.set_xlabel(r'$ \Delta K $', fontsize=15)
    ax.set_ylabel(f'$ Q_{plane} $', fontsize=15)



def ax_errorbar_plot(ax, magnet_df, plane, clean, plot_settings):

    ax.errorbar(return_delta_k(magnet_df.loc[magnet_df.loc[:, f"{CLEANED}{plane}"].values == clean, :]),
                return_Q(magnet_df.loc[magnet_df.loc[:, f"{CLEANED}{plane}"].values == clean, :], plane),
                yerr=return_err_Q(magnet_df.loc[magnet_df.loc[:, f"{CLEANED}{plane}"].values == clean, :], plane),
                color=plot_settings["color"],
                fmt=plot_settings["marker"],
                label=plot_settings["label"],
                zorder=plot_settings["zorder"]
                )
    return ax


def return_delta_k(df):
    return ((df.loc[:, K].dropna() - df.headers[K])*1E3).tolist()


def return_Q(df, plane):
    return (df.loc[:, f"{TUNE}{plane}"].dropna()).tolist()


def return_err_Q(df, plane):
    return (df.loc[:, f"{ERR}{TUNE}{plane}"].dropna()).tolist()


def return_fit_data(df, plane):
    return analysis.fit_prec(
                analysis.return_fit_input(df, plane),
                df.headers[f"{AVERAGE}{BETA}{plane}"]) + df.headers[f"{TUNE}{plane}"]
