"""
Analysis
--------

This module contains the analysis functionality of ``kmod``.
It provides functions to calculate beta functions at different locations from K-modulation data.
"""
import datetime

import numpy as np
import scipy.optimize
import tfs
from tfs import tools as tfstools

from omc3_kmod.definitions import formats
from omc3_kmod.definitions.constants import PLANES
from omc3_kmod.kmod.constants import (CLEANED, K, TUNE, ERR, BETA, STAR, WAIST, PHASEADV, AVERAGE, LSTAR,
    HEADER_MAGNET_LENGTHS, HEADER_MAGNET_LOCATIONS, HEADER_MAGNET_TOTAL_LENGTH,INSTRUMENT_NAMES, INSTRUMENT_LOCATIONS)
from omc3_kmod.model.constants import TWISS_DAT
from omc3_kmod.optics_measurements.constants import PHASE_NAME, EXT
from omc3_kmod.utils import logging_tools

LOG = logging_tools.get_logger(__name__)


def return_sign_for_err(n):
    """
    Creates an array for error calculation, of form:
    [[ 0.  0.  0.]
    [ 1.  0.  0.]
    [-1. -0. -0.]
    [ 0.  1.  0.]
    [-0. -1. -0.]
    [ 0.  0.  1.]
    [-0. -0. -1.]]
    Columns corresponds to error, i.e. first column for `dQ` etc.
    """
    sign = np.zeros((2*n+1, n))

    sign[1::2] = np.eye(n)
    sign[2::2] = -np.eye(n)
    return sign


def propagate_beta_in_drift(beta_waist, drift):
    beta = beta_waist + drift**2/beta_waist
    return beta


def calc_betastar(kmod_input_params, results_df, l_star):

    sign = return_sign_for_err(2)      

    for plane in PLANES:
        betastar = propagate_beta_in_drift((results_df.loc[:, f"{BETA}{WAIST}{plane}"].values + sign[:, 0] * results_df.loc[:, f"{ERR}{BETA}{WAIST}{plane}"].values),
                                           (results_df.loc[:, f"{WAIST}{plane}"].values + sign[:, 1] * results_df.loc[:, f"{ERR}{WAIST}{plane}"].values))
        betastar_err = get_err(betastar[1::2]-betastar[0])

        if kmod_input_params.no_sig_digits:
            results_df[f"{BETA}{STAR}{plane}"], results_df[f"{ERR}{BETA}{STAR}{plane}"] = (betastar[0], betastar_err)
        else:
            results_df[f"{BETA}{STAR}{plane}"], results_df[f"{ERR}{BETA}{STAR}{plane}"] = tfstools.significant_digits(betastar[0], betastar_err, return_floats=True)


    # reindex df to put betastar first
    cols = results_df.columns.tolist()
    cols = [cols[0]]+cols[-4:]+cols[1:-4]
    results_df = results_df.reindex(columns=cols)

    for plane in PLANES:
        results_df[f"{PHASEADV}{plane}"], results_df[f"{ERR}{PHASEADV}{plane}"] = phase_adv_from_kmod(
            l_star, betastar[0], betastar_err,
            results_df.loc[:, f"{WAIST}{plane}"].values,
            results_df.loc[:, f"{ERR}{WAIST}{plane}"].values)

    return results_df


def phase_adv_from_kmod(lstar, betastar, ebetastar, waist, ewaist):
    return _phase_adv_from_kmod_value(lstar, betastar, waist),\
           _phase_adv_from_kmod_err(lstar, betastar, ebetastar, waist, ewaist)


def _phase_adv_from_kmod_value(lstar, betastar, waist):
    return (np.arctan((lstar - waist) / betastar) +
            np.arctan((lstar + waist) / betastar)) / (2 * np.pi)


def _phase_adv_from_kmod_err(lstar, betastar, ebetastar, waist, ewaist):
    numer = (2 * lstar * (betastar ** 2 + lstar ** 2 - waist ** 2) * ebetastar) ** 2
    numer = numer + (4 * betastar * lstar * waist * ewaist) ** 2
    denom = (betastar ** 2 + (lstar - waist) ** 2) ** 2
    denom = denom * (betastar ** 2 + (lstar + waist) ** 2) ** 2
    return np.sqrt(numer / denom) / (2 * np.pi)


def calc_beta_inst(name, position, results_df, kmod_input_params):
    betas = np.zeros((2, 2))
    sign = np.array([[0, 0], [1, 0], [-1, 0], [0, 1], [0, -1]])
    for i, plane in enumerate(PLANES):
        waist = results_df.loc[:, f"{WAIST}{plane}_S_LOCATION"].values

        beta = propagate_beta_in_drift((results_df.loc[:, f"{BETA}{WAIST}{plane}"].values + sign[:, 0] * results_df.loc[:, f"{ERR}{BETA}{WAIST}{plane}"].values),
                                       (np.abs( waist  - position) + sign[:, 1] * results_df.loc[:, f"{ERR}{WAIST}{plane}"].values))
        beta_err = get_err(beta[1::2]-beta[0])
        if kmod_input_params.no_sig_digits:
            betas[i, 0], betas[i, 1] = beta[0], beta_err
        else:
            betas[i, 0], betas[i, 1] = tfstools.significant_digits(beta[0], beta_err, return_floats=True)

    return name, position, betas[0, 0], betas[0, 1], betas[1, 0], betas[1, 1]


def calc_beta_at_instruments(kmod_input_params, results_df):

    beta_instr=[]

    for idx, row in kmod_input_params.instruments.iterrows():

        beta_instr.append(calc_beta_inst(
                row[INSTRUMENT_NAMES], row[INSTRUMENT_LOCATIONS], results_df, kmod_input_params))

    instrument_beta_df = tfs.TfsDataFrame(
        columns=['NAME',
                 'S',
                 f"{BETA}{'X'}",
                 f"{ERR}{BETA}{'X'}",
                 f"{BETA}{'Y'}",
                 f"{ERR}{BETA}{'Y'}",
                 ],
        data=beta_instr)

    return instrument_beta_df


def fit_prec(x, beta_av):
    twopiQ = 2 * np.pi * np.modf(x[1])[0]
    dQ = (1/(2.*np.pi)) * np.arccos(np.cos(twopiQ) -
                                    0.5 * beta_av * x[0] * np.sin(twopiQ)) - np.modf(x[1])[0]
    return dQ


np.vectorize(fit_prec)


def fit_approx(x, beta_av):
    dQ = beta_av*x[0]/(4*np.pi)
    return dQ


np.vectorize(fit_approx)


def average_beta_from_Tune(Q, TdQ, l, Dk):
    """Calculates average beta function in quadrupole from tune change ``TdQ`` and ``delta K``."""

    beta_av = 2 * (1 / np.tan(2 * np.pi * Q) *
              (1 - np.cos(2 * np.pi * TdQ)) + np.sin(2 * np.pi * TdQ)) / (l * Dk)
    return abs(beta_av)


def average_beta_focussing_quadrupole(b, w, L, K, Lstar):

    beta0 = b + ((Lstar - w) ** 2 / b)
    alpha0 = -(Lstar - w) / b
    average_beta = (beta0/2.) * (1 + ((np.sin(2 * np.sqrt(abs(K)) * L)) / (2 * np.sqrt(abs(K)) * L))) \
                   - alpha0 * ((np.sin(np.sqrt(abs(K)) * L)**2) / (abs(K) * L)) \
                   + (1/(2*abs(K))) * ((1 + alpha0**2) / beta0) * \
                   (1 - ((np.sin(2 * np.sqrt(abs(K)) * L)) / (2 * np.sqrt(abs(K)) * L)))

    return average_beta


np.vectorize(average_beta_focussing_quadrupole)


def average_beta_defocussing_quadrupole(b, w, L, K, Lstar):
    beta0 = b + ((Lstar - w) ** 2 / b)
    alpha0 = -(Lstar - w) / b
    average_beta = (beta0/2.) * (1 + ((np.sinh(2 * np.sqrt(abs(K)) * L)) / (2 * np.sqrt(abs(K)) * L))) \
                   - alpha0 * ((np.sinh(np.sqrt(abs(K)) * L)**2) / (abs(K) * L)) \
                   + (1/(2*abs(K))) * ((1 + alpha0**2) / beta0) * \
                   (((np.sinh(2 * np.sqrt(abs(K)) * L)) / (2 * np.sqrt(abs(K)) * L)) - 1)

    return average_beta


np.vectorize(average_beta_defocussing_quadrupole)


def calc_tune(magnet_df):
    for plane in PLANES:
        magnet_df.headers[f"{TUNE}{plane}"] = np.average(magnet_df.where(
            magnet_df[f"{CLEANED}{plane}"])[f"{TUNE}{plane}"].dropna())
    return magnet_df


def calc_k(magnet_df):
    magnet_df.headers[K] = np.average(magnet_df.where(magnet_df[f"{CLEANED}X"])[K].dropna())
    return magnet_df


def return_fit_input(magnet_df, plane):

    x = np.zeros((2, len(magnet_df.where(magnet_df[f"{CLEANED}{plane}"])[K].dropna())))

    sign = 1 if plane == 'X' else -1
    x[0, :] = sign*(
            magnet_df.where(magnet_df[f"{CLEANED}{plane}"])[K].dropna() -
            magnet_df.headers[K]) * magnet_df.headers[HEADER_MAGNET_TOTAL_LENGTH]
    x[1, :] = magnet_df.headers[f"{TUNE}{plane}"]

    return x


def do_fit(magnet_df, plane, use_approx=False):
    if not use_approx:
        fun = fit_prec
    elif use_approx:
        fun = fit_approx
    
    sigma = magnet_df.where(magnet_df[f"{CLEANED}{plane}"])[f"{ERR}{TUNE}{plane}"].dropna()
    if not np.any(sigma):
        sigma = 1.E-22 * np.ones(len(sigma))

    av_beta, av_beta_err = scipy.optimize.curve_fit(
        fun,
        xdata=return_fit_input(magnet_df, plane),
        ydata=magnet_df.where(magnet_df[f"{CLEANED}{plane}"])[
            f"{TUNE}{plane}"].dropna() - magnet_df.headers[f"{TUNE}{plane}"],
        sigma=sigma,
        absolute_sigma=True,
        p0=1
    )
    return np.abs(av_beta[0]), np.sqrt(np.diag(av_beta_err))[0]


def get_av_beta(magnet_df):
    for plane in PLANES:
        magnet_df.headers[f"{AVERAGE}{BETA}{plane}"], magnet_df.headers[f"{ERR}{AVERAGE}{BETA}{plane}"] = do_fit(magnet_df, plane)
    return magnet_df


def check_polarity(magnet1_df, magnet2_df, sign):
    left, right = sign
    return np.sign(magnet1_df.headers[K]) == left and np.sign(magnet2_df.headers[K]) == right


def return_df(magnet_dfs, plane):

    sign = {'X': np.array([1, -1]), 'Y': np.array([-1, 1])}
    magnet1_df, magnet2_df = [df for df in magnet_dfs.values()]

    if check_polarity(magnet1_df, magnet2_df, sign[plane]):
        return magnet1_df, magnet2_df, -1
    elif check_polarity(magnet1_df, magnet2_df, -sign[plane]):
        return magnet2_df, magnet1_df, 1
    else:
        raise ValueError('Both magnets have the same polarity.')


def chi2(x, foc_magnet_df, def_magnet_df, plane, kmod_input_params, sign, BPM_distance, phase_adv_constraint):

    b = x[0]
    w = x[1]

    if BPM_distance != 0.0:
        phase_adv = phase_adv_from_kmod(BPM_distance, b, 0.0, w, 0.0)[0]
        weight = kmod_input_params.phase_weight
    else:
        phase_adv = 0.0
        weight = 0.0
        phase_adv_constraint =[1,1] # small hack to fix division by zero in the phase adv term, TODO: make nicer

    c2 = (1-weight)*(((average_beta_focussing_quadrupole(b, w, foc_magnet_df.headers[HEADER_MAGNET_TOTAL_LENGTH] +
        sign[0] * kmod_input_params.errorL, foc_magnet_df.headers[K] +
        sign[1] * kmod_input_params.errorK * foc_magnet_df.headers[K],
        foc_magnet_df.headers[LSTAR] +
        sign[2] * kmod_input_params.misalignment) -
        foc_magnet_df.headers[f"{AVERAGE}{BETA}{plane}"] +
        sign[3] * foc_magnet_df.headers[f"{ERR}{AVERAGE}{BETA}{plane}"])/(def_magnet_df.headers[f"{AVERAGE}{BETA}{plane}"] + foc_magnet_df.headers[f"{AVERAGE}{BETA}{plane}"])/2.0) ** 2 +
        ((average_beta_defocussing_quadrupole(b, -w, def_magnet_df.headers[HEADER_MAGNET_TOTAL_LENGTH] +
        sign[4] * kmod_input_params.errorL, def_magnet_df.headers[K] +
        sign[5] * kmod_input_params.errorK * def_magnet_df.headers[K],
        def_magnet_df.headers[LSTAR] +
        sign[6] * kmod_input_params.misalignment) -
        def_magnet_df.headers[f"{AVERAGE}{BETA}{plane}"] +
        sign[7] * def_magnet_df.headers[f"{ERR}{AVERAGE}{BETA}{plane}"])/(foc_magnet_df.headers[f"{AVERAGE}{BETA}{plane}"] + def_magnet_df.headers[f"{AVERAGE}{BETA}{plane}"])/2.0) ** 2) + \
        weight*(((phase_adv - (phase_adv_constraint[0]+sign[8]*phase_adv_constraint[1]))/phase_adv_constraint[0])**2)

    return c2


def get_beta_waist(magnet_dfs, kmod_input_params, plane, bpm_ip_distance, phase_adv_constraint):

    n = 9
    sign = return_sign_for_err(n)
    foc_magnet_df, def_magnet_df, _ = return_df(magnet_dfs, plane)
    results = np.zeros((2*n+1, 2))

    for i, s in enumerate(sign):

        def fun(x): return chi2(x, foc_magnet_df, def_magnet_df, plane, kmod_input_params, s, bpm_ip_distance, phase_adv_constraint)
        fitresults = scipy.optimize.minimize(fun=fun,
                                             x0=[kmod_input_params.betastar[plane], kmod_input_params.waist[plane]],
                                             method='nelder-mead',
                                             tol=1E-22)

        results[i, :] = fitresults.x[0], fitresults.x[1]

    beta_waist_err = get_err(results[1::2, 0]-results[0, 0])
    waist_err = get_err(results[1::2, 1]-results[0, 1])

    return results[0, 0], beta_waist_err, results[0, 1], waist_err


def get_err(diff_array):
    return np.sqrt(np.sum(np.square(diff_array)))


def headers_for_df(df):
    LOG.debug('Creating headers for DF')
    
    df.headers['DELTA_K'] = np.max(df[K].rolling(5).mean()) - np.min(df[K].rolling(5).mean()) / 2
    df.headers[HEADER_MAGNET_TOTAL_LENGTH] = sum(df.headers[HEADER_MAGNET_LENGTHS])
    df = calc_tune(df)
    df = calc_k(df)
    
    return df


def get_lstar(dfs):

    magnet_dfs = [tfs.TfsDataFrame(
        {HEADER_MAGNET_LOCATIONS: magnet_df.headers[HEADER_MAGNET_LOCATIONS],    
         HEADER_MAGNET_LENGTHS: magnet_df.headers[HEADER_MAGNET_LENGTHS]}
            ).sort_values(by=HEADER_MAGNET_LOCATIONS) for magnet_df in dfs.values()]


    if all(magnet_dfs[0][HEADER_MAGNET_LOCATIONS] < magnet_dfs[1][HEADER_MAGNET_LOCATIONS]):
        left_df = magnet_dfs[0]
        right_df = magnet_dfs[1]
    elif all(magnet_dfs[1][HEADER_MAGNET_LOCATIONS] < magnet_dfs[0][HEADER_MAGNET_LOCATIONS]):
        right_df = magnet_dfs[0]
        left_df = magnet_dfs[1]

    ip_position = (max(left_df[HEADER_MAGNET_LOCATIONS]) + min(right_df[HEADER_MAGNET_LOCATIONS]))/2.

    lstar = np.abs(ip_position - max(left_df[HEADER_MAGNET_LOCATIONS])) - 0.5*left_df[HEADER_MAGNET_LENGTHS].loc[left_df[HEADER_MAGNET_LOCATIONS].idxmax()]

    for circuit, magnet_df in dfs.items():
        magnet_df.headers[LSTAR] = lstar
        dfs[circuit] = magnet_df

    return dfs, lstar, ip_position


def analyse(magnet_dfs, opt, ip_position, bpm_ip_distance, phase_adv, err_phase_adv):

    LOG.info('Simplex to determine beta waist')
    results = {plane: get_beta_waist(
        magnet_dfs,
        opt,
        plane,
        bpm_ip_distance,
        [phase_adv[plane],
        err_phase_adv[plane]]
        )
        for plane in PLANES}

    results_df = tfs.TfsDataFrame(
        columns=['LABEL',
                 "TIME"],
        data=[np.hstack((opt.label,
                         datetime.datetime.now().strftime(formats.TIME)))])

    for plane in PLANES:
        results_df[f"{BETA}{WAIST}{plane}"] = results[plane][0]
        results_df[f"{ERR}{BETA}{WAIST}{plane}"] = results[plane][1]
        results_df[f"{WAIST}{plane}"] = results[plane][2]
        results_df[f"{ERR}{WAIST}{plane}"] = results[plane][3]
        _,_, sign = return_df(magnet_dfs, plane)
        results_df[f"{WAIST}{plane}_S_LOCATION"] = ip_position + sign * results[plane][2]
        results_df[f"{ERR}{WAIST}{plane}_S_LOCATION"] = results[plane][3]


    return results_df
