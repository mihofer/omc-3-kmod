# <img src="https://raw.githubusercontent.com/pylhc/pylhc.github.io/master/docs/assets/logos/OMC_logo.svg" height="28"> 3


This is slim version of [OMC3](https://github.com/pylhc/omc3) which contains only the scripts necessary to run the K-Modulation analysis.


## Getting Started

The `omc3` package is `Python 3.7+` compatible, but not yet deployed to PyPI.
The best way to install is through pip and VCS:

```bash
git clone https://gitlab.cern.ch/mihofer/omc-3-kmod.git
pip install /path/to/omc3
```

Or simply from the online master branch, which is stable:

```bash
pip install git+https://gitlab.cern.ch/mihofer/omc-3-kmod.git
```

After installing, codes can be run with either `python -m omc3.SCRIPT --FLAG ARGUMENT` or calling path to the `.py` file directly.


## Quality checks

- Unit and accuracy tests are run automatically through CI [Gitlab Workflow](https://github.com/pylhc/omc3/actions).
- Pull requests implementing functionality or fixes are merged into the master branch after passing CI, and a reviewer's approval.

### Changelog

See the [CHANGELOG](CHANGELOG.md) file.

## Related Packages

- [tfs-pandas](https://github.com/pylhc/tfs)
- [sdds-reader](https://github.com/pylhc/sdds)
- [generic parser](https://github.com/pylhc/generic_parser)
- [PyLHC](https://github.com/pylhc/PyLHC)
- [Beta-Beat Source](https://github.com/pylhc/Beta-Beat.src)
- [optics functions](https://github.com/pylhc/optics_functions)

### Hints for Developers

In case you want to contribute to `omc3-kmod`'s development, you should install it in `editable` mode:

```
git clone https://gitlab.cern.ch/mihofer/omc-3-kmod.git
pip install --editable /path/to/omc3-kmod
```

You can install extra dependencies (as defined in `setup.py`) suited to your use case with the following commands:

```
pip install --editable /path/to/omc3-kmod[test]
pip install --editable /path/to/omc3-kmod[test,doc]
pip install --editable /path/to/omc3-kmod[all]
```

Open an issue, make your changes in a branch and submit a pull request.

## Authors

* **pyLHC/OMC-Team** - *Working Group* - [pyLHC](https://github.com/orgs/pylhc/teams/omc-team)

## License
This project is licensed under the `GNU GPLv3` License.
Please take a moment to check its permissivity - see the [LICENSE](LICENSE) file for details.
