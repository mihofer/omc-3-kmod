Correction
**************************

.. automodule:: omc3.correction.constants
    :members:


.. automodule:: omc3.correction.filters
    :members:


.. automodule:: omc3.correction.handler
    :members:


.. automodule:: omc3.correction.model_appenders
    :members:


.. automodule:: omc3.correction.model_diff
    :members:


.. automodule:: omc3.correction.response_madx
    :members:


.. automodule:: omc3.correction.response_twiss
    :members:


.. automodule:: omc3.correction.sequence_evaluation
    :members:


.. automodule:: omc3.correction.response_io
    :members:
